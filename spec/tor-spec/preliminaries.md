<a id="tor-spec.txt-0"></a>

# Preliminaries

<a id="tor-spec.txt-0.1"></a>

## Notation and encoding{#notation-and-encoding}

```text
   KP -- a public key for an asymmetric cipher.
   KS -- a private key for an asymmetric cipher.
   K  -- a key for a symmetric cipher.
   N  -- a "nonce", a random value, usually deterministically chosen
         from other inputs using hashing.
```

## Security parameters

Tor uses a stream cipher, a public-key cipher, the Diffie-Hellman
protocol, and a hash function.

KEY_LEN -- the length of the stream cipher's key, in bytes.

```text
   KP_ENC_LEN -- the length of a public-key encrypted message, in bytes.
   KP_PAD_LEN -- the number of bytes added in padding for public-key
     encryption, in bytes. (The largest number of bytes that can be encrypted
     in a single public-key operation is therefore KP_ENC_LEN-KP_PAD_LEN.)

   DH_LEN -- the number of bytes used to represent a member of the
     Diffie-Hellman group.
   DH_SEC_LEN -- the number of bytes used in a Diffie-Hellman private key (x).
```

## Message lengths {#msg-len}

Some message lengths are fixed in the Tor protocol.
We give them here.
Some of these message lengths depend
on the version of the Tor link protocol in use:
for these, the link protocol is denoted in this table with `v`.

| Name | Length in bytes | Meaning |
| ---- | --------------- | ------- |
| `CELL_BODY_LEN` | 509 | The body length for a [fixed-length cell](./cell-packet-format.md#fixed-length-cell). |
| `CIRCID_LEN(v)`, `v` < 4 | 2 | The length of a [circuit ID](./cell-packet-format.md#circid) |
| `CIRCID_LEN(v)`, `v` ≥ 4 | 4 |  |
| `CELL_LEN(v)`, `v` < 4 | 512 | The length of a [fixed-length cell](./cell-packet-format.md). |
| `CELL_LEN(v)`, `v` ≥ 4 | 514 |  |

Note that for all `v`, `CELL_LEN(v) = 1 + CIRCID_LEN(v) + CELL_BODY_LEN`.

> Formerly `CELL_BODY_LEN` was called sometimes called `PAYLOAD_LEN`.

<a id="tor-spec.txt-0.3"></a>

## Ciphers{#ciphers}

These are the ciphers we use _unless otherwise specified_.  Several of
them are deprecated for new use.

For a stream cipher, unless otherwise specified, we use 128-bit AES in
counter mode, with an IV of all 0 bytes.  (We also require AES256.)

For a public-key cipher, unless otherwise specified, we use RSA with
1024-bit keys and a fixed exponent of 65537.  We use OAEP-MGF1
padding, with SHA-1 as its digest function.  We leave the optional
"Label" parameter unset. (For OAEP padding, see
ftp://ftp.rsasecurity.com/pub/pkcs/pkcs-1/pkcs-1v2-1.pdf)

We also use the Curve25519 group and the Ed25519 signature format in
several places.

For Diffie-Hellman, unless otherwise specified, we use a generator
(g) of 2.  For the modulus (p), we use the 1024-bit safe prime from
rfc2409 section 6.2 whose hex representation is:

```text
     "FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E08"
     "8A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B"
     "302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9"
     "A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE6"
     "49286651ECE65381FFFFFFFFFFFFFFFF"
```

As an optimization, implementations SHOULD choose DH private keys (x) of
320 bits.  Implementations that do this MUST never use any DH key more
than once.
\[May other implementations reuse their DH keys?? -RD\]
\[Probably not. Conceivably, you could get away with changing DH keys once
per second, but there are too many oddball attacks for me to be
comfortable that this is safe. -NM\]

KEY_LEN=16.
DH_LEN=128; DH_SEC_LEN=40.
KP_ENC_LEN=128; KP_PAD_LEN=42.

<span id="random-values">
All "random" values MUST be generated with a cryptographically
strong pseudorandom number generator seeded from a strong entropy
source, unless otherwise noted.
All "random" values MUST selected uniformly at random from the
universe of possible values, unless otherwise noted.
</span>

## Cryptographic hash functions {#hash}

Tor uses the cryptographic hash functions
SHA-1, SHA-256, and SHA3-256.

> SHA-1 is vulnerable to various collision attacks,
> and should not be used anywhere new.
> Its existing applications are redundant
> with other hash functions,
> deprecated, or both.

We denote applications of these hash functions to some message M as:
 * `SHA1(M)`
 * `SHA256(M)`
 * `SHA3_256(M)`.

We define constants to represent the lengths in bytes
of the digests that these functions output:

```
SHA1_LEN = 20
SHA256_LEN = 32
SHA3_256_LEN = 32
```

Note that although the above terminology is preferred,
many of our older specifications have not yet been converted to use it.
In some places, we also use `H(M)` to mean "the digest of M",
and `DIGEST_LEN` or `HASH_LEN` to refer to the length of that digest.
Unless otherwise specified, `H(M)` is computed using SHA-1.

### Computing the digest of an RSA key {#rsa-digest}

When `key` is an RSA public key,
we use the notation
`DER(key)`
to denote the ASN.1 DER encoding of the key's
representation as a PKCS#1 RSAPublicKey object.

Some older text does not use yet this notation.
When we refer to "the digest of an RSA public key",
unless otherwise specified,
we mean a digest of `DER(key)`.
The hash function should be specified explicitly.
