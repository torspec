<a id="tor-spec.txt-6.4"></a>

# Remote hostname lookup

To find the address associated with a hostname,
the client sends a RELAY_RESOLVE message
containing the hostname to be resolved with a NUL terminating byte.

For a reverse lookup, the client sends
a RELAY_RESOLVE message containing an in-addr.arpa address.

The relay replies with a RELAY_RESOLVED message
containing any number of answers.
Each answer is of the form:

```text
       Type   (1 octet)
       Length (1 octet)
       Value  (variable-width)
       TTL    (4 octets)
   "Length" is the length of the Value field.
   "Type" is one of:

      0x00 -- Hostname
      0x04 -- IPv4 address
      0x06 -- IPv6 address
      0xF0 -- Error, transient
      0xF1 -- Error, nontransient
```

If any answer has a type of 'Error',
then no other answer may be given.

The 'Value' field encodes the answer:

 * IP addresses are given in network order.

 * Hostnames are given in standard DNS order ("www.example.com")
   and not NUL-terminated.

 * The content of Errors is currently ignored. Relays currently
   set it to the string "Error resolving hostname" with no
   terminating NUL. Implementations MUST ignore this value.

For backward compatibility, if there are any IPv4 answers,
one of those must be given as the first answer.

The RELAY_RESOLVE messge must use a nonzero, distinct streamID;
the corresponding RELAY_RESOLVED message must use the same streamID.
No stream is actually created by the relay when resolving the name.
