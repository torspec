<a id="tor-spec.txt-9"></a>

# Subprotocol versioning

This section specifies the Tor subprotocol versioning. They are broken down
into different types with their current version numbers. Any new version
number should be added to this section.

The dir-spec.txt details how those versions are encoded. See the
"proto"/"pr" line in a descriptor and the "recommended-relay-protocols",
"required-relay-protocols", "recommended-client-protocols" and
"required-client-protocols" lines in the vote/consensus format.

Here are the rules a relay and client should follow when encountering a
protocol list in the consensus:

 - When a relay lacks a protocol listed in recommended-relay-protocols,
   it should warn its operator that the relay is obsolete.

 - When a relay lacks a protocol listed in required-relay-protocols, it
   should warn its operator as above. If the consensus is newer than the
   date when the software was released or scheduled for release, it must
   not attempt to join the network.

 - When a client lacks a protocol listed in recommended-client-protocols,
   it should warn the user that the client is obsolete.

 - When a client lacks a protocol listed in required-client-protocols,
   it should warn the user as above.  If the consensus is newer than the
   date when the software was released, it must not connect to the
   network.  This implements a "safe forward shutdown" mechanism for
   zombie clients.

 - If a client or relay has a cached consensus telling it that a given
   protocol is required, and it does not implement that protocol, it
   SHOULD NOT try to fetch a newer consensus.

Software release dates SHOULD be automatically updated as part of the
release process, to prevent forgetting to move them forward.  Software
release dates MAY be manually adjusted by maintainers if necessary.

Starting in version 0.2.9.4-alpha, the initial required protocols for
clients that we will Recommend and Require are:

```text
Cons=1-2 Desc=1-2 DirCache=1 HSDir=1 HSIntro=3 HSRend=1 Link=4
LinkAuth=1 Microdesc=1-2 Relay=2
```

For relays we will Require:

```text
Cons=1 Desc=1 DirCache=1 HSDir=1 HSIntro=3 HSRend=1 Link=3-4
LinkAuth=1 Microdesc=1 Relay=1-2
```

For relays, we will additionally Recommend all protocols which we
recommend for clients.

<a id="tor-spec.txt-9.1"></a>

## "Link"

The "link" protocols are those used by clients and relays to initiate and
receive relay connections and to handle cells on relay connections. The "link"
protocol versions correspond 1:1 to those versions.

Two Tor instances can make a connection to each other only if they have at
least one link protocol in common.

The current "link" versions are: "1" through "5". See
[Negotiating versions with VERSIONS cells](./negotiating-channels.md#VERSIONS-cells) for more
information. All current Tor versions support "1-3"; versions from
0.2.4.11-alpha and on support "1-4"; versions from 0.3.1.1-alpha and on
support "1-5". Eventually we will drop "1" and "2".

<a id="tor-spec.txt-9.2"></a>

## "LinkAuth" {#link-auth}

LinkAuth protocols correspond to varieties of AUTHENTICATE cells used for
the v3+ link protocols.

Current versions are:

 * "1" (`LINKAUTH_RSA_SHA256_TLSSECRET`) --
   the RSA link authentication described in [Link authentication type 1: RSA-SHA256-TLSSecret](./obsolete-channels.md#RSA-SHA256-TLSSecret).

 * "2" is unused, and reserved by [proposal 244].

 * "3" (`LINKAUTH_ED25519_SHA256_EXPORTER`) --
   the ed25519 link authentication described in [Link authentication type 3: Ed25519-SHA256-RFC5705](./negotiating-channels.md#Ed25519-SHA256-RFC5705).

<a id="tor-spec.txt-9.3"></a>

## "Relay"

The "relay" protocols are those used to handle CREATE/CREATE2
cells, and those that handle the various relay messages received
after a CREATE/CREATE2 cell.  (Except, relay cells used to manage
introduction and rendezvous points are managed with the "HSIntro"
and "HSRend" protocols respectively.)

Current versions are as follows.

 * "1" (`RELAY_BASE`) -- supports the TAP key exchange, with all features in Tor 0.2.3.
   Support for CREATE and CREATED.

 * "2" (`RELAY_NTOR`) --
   supports the ntor key exchange, and all features in Tor
   0.2.4.19.  Includes support for CREATE2 and CREATED2 and
   EXTEND2 and EXTENDED2.

   Relay=2 has limited IPv6 support:

    * Clients might not include IPv6 ORPorts in EXTEND2 messages.
    * Relays (and bridges) might not initiate IPv6 connections in
      response to EXTEND2 messages containing IPv6 ORPorts, even if they
      are configured with an IPv6 ORPort.

   However, relays support accepting inbound connections to their IPv6
   ORPorts. And they might extend circuits via authenticated IPv6
   connections to other relays.

 * "3" (`RELAY_EXTEND_IPv6`) --
   relays support extending over IPv6 connections in response to an
   EXTEND2 message containing an IPv6 ORPort.

   Bridges might not extend over IPv6, because they try to imitate
   client behaviour.

   A successful IPv6 extend requires:
     * Relay subprotocol version 3 (or later) on the extending relay,
     * an IPv6 ORPort on the extending relay,
     * an IPv6 ORPort for the accepting relay in the EXTEND2 message, and
     * an IPv6 ORPort on the accepting relay.
   (Because different tor instances can have different views of the
   network, these checks should be done when the path is selected.
   Extending relays should only check local IPv6 information, before
   attempting the extend.)

   When relays receive an EXTEND2 message containing both an IPv4 and an
   IPv6 ORPort, and there is no existing authenticated connection with
   the target relay, the extending relay may choose between IPv4 and
   IPv6 at random. The extending relay might not try the other address,
   if the first connection fails.

   As is the case with other subprotocol versions, tor advertises,
   recommends, or requires support for this protocol version, regardless
   of its current configuration.

   In particular:
     * relays without an IPv6 ORPort, and
     * tor instances that are not relays,
   have the following behaviour, regardless of their configuration:
     * advertise support for "Relay=3" in their descriptor
       (if they are a relay, bridge, or directory authority), and
     * react to consensuses recommending or requiring support for
       "Relay=3".

   This subprotocol version is described in
   [proposal 311], and implemented in Tor 0.4.5.1-alpha.

 * "4" (`RELAY_NTORV3`) --
   support the ntorv3 (version 3) key exchange and all features in
   0.4.7.3-alpha. This adds a new CREATE2 cell type. See [proposal 332]
   and [The "ntor-v3" handshake](./create-created-cells.md#ntor-v3) for more details.

 * "5" (`RELAY_NEGOTIATE_SUBPROTO`) \[RESERVED\] --
    support the ntorv3 subprotocol request extension
   ([proposal 346])
   allowing a client to request what features to be used on a circuit.

<a id="tor-spec.txt-9.4"></a>

## "HSIntro" {#HSIntro}

The "HSIntro" protocol handles introduction points.

 * "3" (`HSINTRO_V2`) --
   supports the RSA-based introduction point protocol
   of [proposal 121] in Tor 0.2.1.6-alpha.

 * "4" (`HSINTRO_V3`) --
   support ed25519-based HS v3 introduction point protocol as defined by
   [proposal 224] in Tor 0.3.0.4-alpha.

 * "5" (`HSINTRO_RATELIM`) --
   support ESTABLISH_INTRO message DoS parameters extension for onion
   service version 3 only in Tor 0.4.2.1-alpha.

<a id="tor-spec.txt-9.5"></a>

## "HSRend"

The "HSRend" protocol handles rendezvous points.

 * "1" (`HSREND_V2`) --
   supports all features in Tor 0.0.6.

 * "2" (`HSREND_V3`) --
   supports RENDEZVOUS2 messages of arbitrary length as long as they
   have 20 bytes of cookie in Tor 0.2.9.1-alpha.

<a id="tor-spec.txt-9.6"></a>

## "HSDir"

The "HSDir" protocols are the set of hidden service document types that can
be uploaded to, understood by, and downloaded from a tor relay, and the set
of URLs available to fetch them.

 * "1" (`HSDIR_V2`) --
   supports all features in Tor 0.2.0.10-alpha.

 * "2" (`HSDIR_V3`) --
   support ed25519 blinded keys request which is defined by the HS v3
   protocol as part of [proposal 224] in Tor 0.3.0.4-alpha.

<a id="tor-spec.txt-9.7"></a>

## "DirCache"

The "DirCache" protocols are the set of documents available for download
from a directory cache via BEGIN_DIR, and the set of URLs available to
fetch them.  (This excludes URLs for hidden service objects.)

 * "1" (`DIRCACHE_BASE`) --
   supports all features in Tor 0.2.4.19.

 * "2" (`DIRCACHE_CONSDIFF`) --
   adds support for consensus diffs in Tor 0.3.1.1-alpha.

<a id="tor-spec.txt-9.8"></a>

## "Desc"

Describes features present or absent in descriptors.

Most features in descriptors don't require a "Desc" update -- only those
that need to someday be required.  For example, someday clients will need
to understand ed25519 identities.

 * "1" (`DESC_BASE`) --
   supports all features in Tor 0.2.4.19.

 * "2" (`DESC_CROSSSIGN`) --
   cross-signing with onion-keys, signing with ed25519
   identities.

 * "3" (`DESC_NO_TAP`) --
   parsing relay descriptors without onion-keys;
   generating them when the `publish-dummy-tap-key` option is `0`.

 * "4" -- Reserved to indicate support for
          understanding and building paths according to
          [happy families](../proposals/321-happy-families.md).

<a id="tor-spec.txt-9.9"></a>

## "Microdesc"

Describes features present or absent in microdescriptors.

Most features in descriptors don't require a "MicroDesc" update -- only
those that need to someday be required.  These correspond more or less with
consensus methods.

 * "1" (`MICRODESC_BASE`) --
   consensus methods 9 through 20.

 * "2" (`MICRODESC_ED25519_KEY`) --
   consensus method 21 (adds ed25519 keys to microdescs).

 * "3" (`MICRODESC_NO_TAP`) --
   Accepts Microdescriptors without onion-key bodies.
   (Consensus method TBD; see [proposal 350].)

<a id="tor-spec.txt-9.10"></a>

## "Cons"

Describes features present or absent in consensus documents.

Most features in consensus documents don't require a "Cons" update -- only
those that need to someday be required.

These correspond more or less with consensus methods.

 * "1" (`CONS_BASE`) --
   consensus methods 9 through 20.

 * "2" (`CONST_ED25519_MDS`) --
   consensus method 21 (adds ed25519 keys to microdescs).

<a id="tor-spec.txt-9.11"></a>

## "Padding"

Describes the padding capabilities of the relay.

 * "1" \[DEFUNCT\] --
   Relay supports circuit-level padding. This version MUST NOT
   be used as it was also enabled in relays that don't actually support
   circuit-level padding. Advertised by Tor versions from
   tor-0.4.0.1-alpha and only up to and including tor-0.4.1.4-rc.

 * "2" (`PADDING_MACHINES_CIRC_SETUP`) --
   Relay supports the HS circuit setup padding machines ([proposal 302]).
   Advertised by Tor versions from tor-0.4.1.5 and onwards.

<a id="tor-spec.txt-9.12"></a>

## "FlowCtrl"

Describes the flow control protocol at the circuit and stream level. If
there is no FlowCtrl advertised, tor supports the unauthenticated flow
control features (version 0).

 * "1" (`FLOWCTRL_AUTH_SENDME`) --
   supports authenticated circuit level SENDMEs as of [proposal 289] in
   Tor 0.4.1.1-alpha.

 * "2" (`FLOWCTRL_CC`) --
   supports congestion control by the Exits which implies a new SENDME
   format and algorithm. See [proposal 324] for more details. Advertised
   in tor 0.4.7.3-alpha.

## "Conflux"

Describes the communications mechanisms used to bundle circuits together,
in order to split traffic across multiple paths.

 * "1" (`CONFLUX_BASE`) --
   Supports the base coflux protocol from [proposal 329].

<a id="tor-spec.txt-9.13"></a>

## "Datagram"

Describes the UDP protocol capabilities of a relay.

 * "1" (`DATAGRAM_BASE`) \[RESERVED\] --
   supports UDP by an Exit as in the relay command
   CONNECT_UDP, CONNECTED_UDP and DATAGRAM. See [proposal 339] for more details.
   (Not yet advertised, reserved)

[proposal 121]: ../proposals/121-hidden-service-authentication.txt
[proposal 224]: ../proposals/224-rend-spec-ng.txt
[proposal 244]: ../proposals/244-use-rfc5705-for-tls-binding.txt
[proposal 289]: ../proposals/289-authenticated-sendmes.txt
[proposal 302]: ../proposals/302-padding-machines-for-onion-clients.txt
[proposal 311]: ../proposals/311-relay-ipv6-reachability.txt
[proposal 324]: ../proposals/324-rtt-congestion-control.txt
[proposal 329]: ../proposals/329-traffic-splitting.md
[proposal 332]: ../proposals/332-ntor-v3-with-extra-data.md
[proposal 339]: ../proposals/339-udp-over-tor.md
[proposal 346]: ../proposals/346-protovers-again.md
[proposal 350]: ../proposals/350-remove-tap.md

