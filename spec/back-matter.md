# About the Tor Specifications documents

The canonical, official, versions of these documents are on the
[Tor Specifications website](https://spec.torproject.org/)
maintained by the [Tor Project](https://www.torproject.org/).

Only the Tor Specifications themselves are approved.
The [Proposals](../proposals/) are, by their nature, drafts.

When linking to the Specifications,
consider using one of the links advertised in the
[Table of Permalinks](permalinks.html).

## Source code

The Specifications and Proposals are maintained by the Tor Project
in a
[gitlab repository](https://gitlab.torproject.org/tpo/core/torspec/).

Corrections and clarifications are welcome.
To propose a change to the Tor protocol, use the
[Proposals process](proposals/001-process.txt)

## Building

The documents are in Markdown and formatted with
[mdbook](https://rust-lang.github.io/mdBook/).
To build the formatted HTML:

```text,ignore
cargo install mdbook
git clone https://gitlab.torproject.org/tpo/core/torspec/
cd torspec
bin/build_html
```

The output is then in `html/`.

## Source code structure, and output webtree

There are two mdbook books here:

 * **The Tor Specifications**: source code in `specs/`,
   formatted output in `html/`.

 * **Proposals**: source code in `proposals/`,
   formatted output in `html/proposals/`.

Each book's source files are listed,
and the chapter defined,
in its `SUMMARY.md`.
The format is pretty restrictive;
see the
[mdbook documentation](https://rust-lang.github.io/mdBook/format/summary.html).

## Editing advice

To edit these specs, clone the
[git repository](https://gitlab.torproject.org/tpo/core/torspec/)
and edit the
appropriate file in the `spec` directory.  These files will match
the URLs of their corresponding pages, so if you want to edit
`tor-spec/flow-control.html`,
you'll be looking for a file
called `spec/tor-spec/flow-control.md`.

We have started a [style guide](./STYLE.md) for writing new parts of
this spec; as of 2023 it is quite preliminary. You should feel free to
edit it!

