# Historical protocol elements and behaviours

## Scope and introduction

This chapter documents historical protocol elements
and historical behaviours.

**Note**:
the conventions set out here are not yet followed
throughout the Tor specifications.

### Definition of "Historical"

A **historical** protocol element or behaviour
is one which is neither generated/exhibited,
nor recognised/consumed/handled,
by any supported version of the Tor implementations.

It should not be necessary to consider historical information
to understand the behaviour of the live Tor network.
It may be necessary to consider it for some special purposes:
for example, as part of developing
tools for processing historical data (eg, old consensuses);
or,
tools which use probing to try to determine the
underlying version of possibly mendacious remote software.

If *any* supported Tor implementation generates or handles a behavior,
it should be described in the main body,
*not* relegated to Historical status.
Such a behaviour
must be understood to understand the existing live network,
and its implementations,
even though it may be obsolete or deprecated.


### Representation of historical material

Where practical,
we move historical material out of the mainline text.

We then document the historical elements here.
The structure of this chapter
approximately mirrors (as a subset)
the structure of the main, current, parts of the spec.
Each historical protocol element is placed in its appropriate context.

When that isn't convenient,
historical material remains in the main text.
In this case we
mark it non-normative,
and introduce it with a "Historical" subheading.
