# Historical netdoc Items

## Historical Server Descriptor Items

These items once appeared in
[server descriptors](../dir-spec/server-descriptor-format.html)

Items are listed in approximate reverse order of withdrawal,
oldest last.

### `protocols`

```text
   "protocols" SP "Link" SP LINK-VERSION-LIST SP "Circuit" SP
          CIRCUIT-VERSION-LIST NL

       [At most once.]
```

An obsolete list of protocol versions, superseded by the "proto"
entry.  This list was never parsed, and has not been emitted
since Tor 0.2.9.4-alpha. New code should neither generate nor
parse this line.

### `read-history`, `write-history`

```text
    "read-history" YYYY-MM-DD HH:MM:SS (NSEC s) NUM,NUM,NUM,NUM,NUM... NL
        [At most once]
    "write-history" YYYY-MM-DD HH:MM:SS (NSEC s) NUM,NUM,NUM,NUM,NUM... NL
        [At most once]
```

(These fields once appeared in router descriptors,
but have appeared in
[extra-info descriptors](../dir-spec/extra-info-document-format.html)
since 0.2.0.x.)
