<a id="rend-spec-v3.txt-G"></a>

# Appendix G: Managing authorized client data \[RESTRICTED-DISCOVERY-MGMT\]

Hidden services and clients can configure their authorized client data either
using the torrc, or using the control port. This section presents a suggested
scheme for configuring restricted discovery. Please see appendix
\[HIDSERVDIR-FORMAT\] for more information about relevant hidden service files.

(NOTE: restricted discovery is implemented as of 0.3.5.1-alpha.)

G.1. Configuring restricted discovery using torrc

G.1.1. Hidden Service side configuration

> Note: "restricted discovery" is called "client authorization" in the C Tor
> implementation

```text
     A hidden service that wants to enable client authorization, needs to
     populate the "authorized_clients/" directory of its HiddenServiceDir
     directory with the ".auth" files of its authorized clients.

     When Tor starts up with a configured onion service, Tor checks its
     <HiddenServiceDir>/authorized_clients/ directory for ".auth" files, and if
     any recognized and parseable such files are found, then client
     authorization becomes activated for that service.

  G.1.2. Service-side bookkeeping

     This section contains more details on how onion services should be keeping
     track of their client ".auth" files.

     For the "descriptor" authentication type, the ".auth" file MUST contain
     the x25519 public key of that client. Here is a suggested file format:

        <auth-type>:<key-type>:<base32-encoded-public-key>

     Here is an an example:

        descriptor:x25519:OM7TGIVRYMY6PFX6GAC6ATRTA5U6WW6U7A4ZNHQDI6OVL52XVV2Q

     Tor SHOULD ignore lines it does not recognize.
     Tor SHOULD ignore files that don't use the ".auth" suffix.

  G.1.3. Client side configuration

     A client who wants to register client authorization data for onion
     services needs to add the following line to their torrc to indicate the
     directory which hosts ".auth_private" files containing client-side
     credentials for onion services:

         ClientOnionAuthDir <DIR>

     The <DIR> contains a file with the suffix ".auth_private" for each onion
     service the client is authorized with. Tor should scan the directory for
     ".auth_private" files to find which onion services require client
     authorization from this client.

     For the "descriptor" auth-type, a ".auth_private" file contains the
     private x25519 key:

        <onion-address>:descriptor:x25519:<base32-encoded-privkey>

     The keypair used for client authorization is created by a third party tool
     for which the public key needs to be transferred to the service operator
     in a secure out-of-band way. The third party tool SHOULD add appropriate
     headers to the private key file to ensure that users won't accidentally
     give out their private key.

  G.2. Configuring client authorization using the control port

  G.2.1. Service side

     A hidden service also has the option to configure authorized clients
     using the control port. The idea is that hidden service operators can use
     controller utilities that manage their access control instead of using
     the filesystem to register client keys.

     Specifically, we require a new control port command ADD_ONION_CLIENT_AUTH
     which is able to register x25519/ed25519 public keys tied to a specific
     authorized client.
      [XXX figure out control port command format]

     Hidden services who use the control port interface for client auth need
     to perform their own key management.

  G.2.2. Client side

     There should also be a control port interface for clients to register
     authorization data for hidden services without having to use the
     torrc. It should allow both generation of client authorization private
     keys, and also to import client authorization data provided by a hidden
     service

     This way, Tor Browser can present "Generate client auth keys" and "Import
     client auth keys" dialogs to users when they try to visit a hidden service
     that is protected by client authorization.

     Specifically, we require two new control port commands:
                   IMPORT_ONION_CLIENT_AUTH_DATA
                   GENERATE_ONION_CLIENT_AUTH_DATA
     which import and generate client authorization data respectively.

     [XXX how does key management work here?]
     [XXX what happens when people use both the control port interface and the
          filesystem interface?]
```
