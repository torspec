# Downloading information from other directory authorities

## Downloading missing certificates from other directory authorities { #download-missing-certs }

XXX when to download certificates.

<a id="dir-spec.txt-3.6"></a>

## Downloading server descriptors from other directory authorities { #download-missing-descs }

Periodically (currently, every 10 seconds), directory authorities check
whether there are any specific descriptors that they do not have and that
they are not currently trying to download.
Authorities identify them by hash in vote (if publication date is more
recent than the descriptor we currently have).

\[XXXX need a way to fetch descriptors ahead of the vote?  v2 status docs can
do that for now.\]

If so, the directory authority launches requests to the authorities for these
descriptors, such that each authority is only asked for descriptors listed
in its most recent vote.  If more
than one authority lists the descriptor, we choose which to ask at random.

If one of these downloads fails, we do not try to download that descriptor
from the authority that failed to serve it again unless we receive a newer
network-status (consensus or vote) from that authority that lists the same
descriptor.

```text
   Directory authorities must potentially cache multiple descriptors for each
   router. Authorities must not discard any descriptor listed by any recent
   consensus.  If there is enough space to store additional descriptors,
   authorities SHOULD try to hold those which clients are likely to download the
   most.  (Currently, this is judged based on the interval for which each
   descriptor seemed newest.)
[XXXX define recent]
```

Authorities SHOULD NOT download descriptors for routers that they would
immediately reject for reasons listed in section 3.2.

<a id="dir-spec.txt-3.7"></a>

## Downloading extra-info documents from other directory authorities { #download-missing-extrainfo }

Periodically, an authority checks whether it is missing any extra-info
documents: in other words, if it has any server descriptors with an
extra-info-digest field that does not match any of the extra-info
documents currently held.  If so, it downloads whatever extra-info
documents are missing.  We follow the same splitting and back-off rules
as in section 3.6.

<a id="dir-spec.txt-3.8"></a>
