```
Filename: 352-complex-dns-for-vpn.md
Title: Handling Complex DNS Traffic for VPN usage in Tor
Author: Alexander Færøy
Created: 2024-05-22
Status: Draft
```

# Introduction

As part of our work on building a prototype for a VPN client that uses the Tor
network, we have investigated need for more modern DNS handling within the Tor
network.

Today, the Tor exit nodes supports resolving hostnames and reverse DNS for
either IPv4 or IPv6. We handle reverse DNS by sending a `RELAY_RESOLVE` cell
containing an `in-addr.arpa` hostname and receives a hostname in the returned
cell upon success.

The goal with this proposal is to suggest support for a wider set of DNS
requests to be resolved via the Tor network, and how adding support for this
impacts the network in various ways.

This work was originally planned to be carried out directly with a larger set of
of features, but with the arrival of proposal #348 (UDP Application Support in
Tor), we have been able to experiment more directly with the needed DNS types on
the Android platform directly using both TCP and UDP resolvers.

This proposal is largely designed for users that uses Tor using the `DNSPort` or
via Onionmasq's virtual DNS proxy. This means that we do not worry about the
additional round-trip for DNS requests and responses here as they are expected
in the VPN context.

This proposal doesn't currently specify the actual encoding or advertising of
any of these changes and will be revised later when needed.

# New Records

In this section, we discuss the new DNS records that we propose should be added
to the Tor protocol's DNS layer. All of these requires updates to
`tor-spec.txt`'s section 6.4 on "Remote hostname lookup" where the new types
needs to be added in addition to the response format. We note that as long as
fragmented cells aren't yet supported in C Tor, the responses here may be
truncated due to Tor's cell limit size. We will be expanding upon [proposal 219]
and [proposal 317] as possible options for the encoding of the new record types.

It's important that we ensure that Tor's DNSPort and/or Onionmasq's virtual VPN
DNS server continues to let people use known common tools (such as `dig` or
`drill`) to debug the DNS layer in Tor.

## HTTPS and SVCB Records

In RFC 9460, the IETF defines the SVCB and HTTPS DNS resource record's (RR).

These are important in the VPN context to allow web application authors to both
provide alternative port's to connect to for their HTTP resources, but more
importantly, it can be used together with modern HTTP implementation API's to
send pre-flight requests to see if a given end-point supports HTTP/3
(HTTP-over-Quic/UDP) as an alternative to HTTP-over-TCP. This is needed for
additional usage of the UDP feature defined in proposal #348.

The wire format is defined in RFC 9460 section 2.2, but no wire parsing should
be needed in Tor as we simply need to propagate the response payload to the
client.

Tor is currently limited to the 509 bytes, which may be an issue for very
complicated payloads, but we SHOULD trim those until the network support
fragmented cells everywhere.

# Security Implications

DNS is a potential attack vector for adversaries interested in causing harm for
the Tor network.

A major risk involving DNS is Denial of Service attacks. In particular,
amplification attacks are a powerful way where an adversary with relatively
little effort and request size can cause an exit node to be flooded with
significantly larger response sizes from its DNS provider, thus blocking its
uplink with DNS responses instead of more useful traffic. Tor's relatively
conservative support of the DNS protocol has long caused this to not be a major
issue, but we have in the past seen DDoS attacks against the Tor network that
involved flooding exit nodes with DNS requests.

Since we are adding support for new types where the request-to-response ratio is
different from the A, AAAA, and PTR lookups we support in Tor today, we need to
be cautious and ensure that we can disable these features and keep track of the
request-to-response ratio in the network.

Because of the above, it is important that when this proposal is implemented,
that we ensure the following:

1. We must make sure to use Tor's internal MetricsPort features to keep track of
   both the request type, the request size, and the response sizes. This will
   allow us to analyze together with the Exit node operator community if some of
   the DNS types are more prone to abuse than others.

   We suggest the following records to be exposed via the MetricsPort (naming
   can be changed later):

   `dns_lookup_count{type=rr} = n`
   `dns_lookup_request_size{type=rr} = n`
   `dns_lookup_response_size{type=rr} = n`

    Where `rr` is all of A, AAAA, PTR, and SVCB/HTTPS.

2. We should ensure that the directory authorities have the ability to disable
   specific DNS types globally in the network in case of catastrophic abuse of
   the feature. This can be done using consensus parameters.

# External References

1. Cloudflare wrote a blog post of the usage of HTTPS records in
   https://blog.cloudflare.com/speeding-up-https-and-http-3-negotiation-with-dns

2. PowerDNS already implemented automatic hints for SVCB records in their
   software. See:
   https://doc.powerdns.com/authoritative/guides/svcb.html#svc-autohints

# Testing and Debugging

Cloudflare has SVCB enabled on `blog.cloudflare.com` and
`crypto.cloudflare.com`. Their record can be seen using:

    $ dig blog.cloudflare.com -t TYPE65

If you have `drill` installed with modern HTTPS/SVCB support, you can also query
it with a nicer response using:

    $ drill HTTPS blog.cloudflare.com

This should make it easier to test the feature.

[proposal 219]: ../proposals/219-expanded-dns.txt
[proposal 317]: ../proposals/317-secure-dns-name-resolution.txt
